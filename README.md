# Capo Controller #

An Android application for controlling the Capo robot over Bluetooth.

Please set up the [Capo Bluetooth Server](https://bitbucket.org/capo-mbed/capobluetoothserver) before using.

### Important places to visit next ###

* [Project Capo main repository](https://github.com/project-capo)
* [Project Capo docs](http://capo-docs.readthedocs.io/pl/english/)