package pl.edu.agh.sw16.capocontroller;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ConnectActivity extends Activity {
    private static final int ENABLE_BT_REQUEST = 1;

    public static final String EXTRA_SELECTED_DEVICE =
            ConnectActivity.class.getPackage() + ".extra.SELECTED_DEVICE";

    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mReceiver;

    private List<DeviceListItem> mPairedDevices;
    private ArrayAdapter<DeviceListItem> mPairedDevicesAdapter;

    private List<DeviceListItem> mNewDevices;
    private ArrayAdapter<DeviceListItem> mNewDevicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        initDevicesLists();

        registerBtDeviceFoundReceiver();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST);
        } else {
            showPairedDevices();
            mBluetoothAdapter.startDiscovery();
        }
    }

    private void registerBtDeviceFoundReceiver() {
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                        mNewDevices.add(new DeviceListItem(device));
                        mNewDevicesAdapter.notifyDataSetChanged();
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.cancelDiscovery();
        }

        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ENABLE_BT_REQUEST) {
            if (resultCode == RESULT_OK) {
                showPairedDevices();
                mBluetoothAdapter.startDiscovery();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    private void initDevicesLists() {
        mPairedDevices = new ArrayList<>();
        mPairedDevicesAdapter = new ArrayAdapter<>(this, R.layout.list_item_device, R.id.list_item_device_textview, mPairedDevices);
        final ListView pairedDevicesListView = (ListView) findViewById(R.id.listview_paired_devices);
        pairedDevicesListView.setAdapter(mPairedDevicesAdapter);

        pairedDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBluetoothAdapter.cancelDiscovery();
                DeviceListItem selected = (DeviceListItem) pairedDevicesListView.getItemAtPosition(position);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_SELECTED_DEVICE, selected.getDevice().getAddress());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        mNewDevices = new ArrayList<>();
        mNewDevicesAdapter = new ArrayAdapter<>(this, R.layout.list_item_device, R.id.list_item_device_textview, mNewDevices);
        final ListView newDevicesListView = (ListView) findViewById(R.id.listview_new_devices);
        newDevicesListView.setAdapter(mNewDevicesAdapter);

        newDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBluetoothAdapter.cancelDiscovery();
                DeviceListItem selected = (DeviceListItem) newDevicesListView.getItemAtPosition(position);
                BluetoothDevice device = selected.getDevice();
                boolean isBonded = device.createBond();
                if (isBonded) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(EXTRA_SELECTED_DEVICE, device.getAddress());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Toast.makeText(ConnectActivity.this, R.string.device_bond_error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showPairedDevices() {
        for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
            mPairedDevices.add(new DeviceListItem(device));
        }
        mPairedDevicesAdapter.notifyDataSetChanged();
    }

    private class DeviceListItem {
        private final BluetoothDevice device;

        DeviceListItem(BluetoothDevice device) {
            this.device = device;
        }

        BluetoothDevice getDevice() {
            return device;
        }

        @Override
        public String toString() {
            return device.getName() + "\n" + device.getAddress();
        }
    }
}
