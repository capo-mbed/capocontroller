package pl.edu.agh.sw16.capocontroller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import java.util.Locale;

import pl.edu.agh.sw16.capocontroller.connection.BluetoothRfcommConnection;
import pl.edu.agh.sw16.capocontroller.connection.CapoConnection;


public class SensorService extends Service implements SensorEventListener {

    private static final String TAG = SensorService.class.getName();

    public static final String ACTION_REGISTER_RECEIVER =
            SensorService.class.getPackage() + ".action.REGISTER_DEVICE";
    public static final String ACTION_STOP_TRANSMISSION =
            SensorService.class.getPackage() + ".action.STOP_TRANSMISSION";
    public static final String ACTION_POSITIVE_ACCELERATION =
            SensorService.class.getPackage() + ".action.POSITIVE_ACCELERATION";
    public static final String ACTION_NEGATIVE_ACCELERATION =
            SensorService.class.getPackage() + ".action.NEGATIVE_ACCELERATION";
    public static final String ACTION_STOP_ROBOT =
            SensorService.class.getPackage() + ".action.STOP_ROBOT";

    public static final String EXTRA_RECEIVER =
            SensorService.class.getPackage() + ".extra.RECEIVER";
    public static final String EXTRA_SENSOR_TYPE =
            SensorService.class.getPackage() + ".extra.SENSOR_TYPE";
    public static final String EXTRA_SENSOR_DELAY =
            SensorService.class.getPackage() + ".extra.SENSOR_DELAY";

    public static final int CODE_START_TRANSMISSION = 1;
    public static final int CODE_STOP_TRANSMISSION = 2;

    private static final int SENSOR_TYPE_DEFAULT = Sensor.TYPE_ACCELEROMETER;
    private static final int SENSOR_DELAY_DEFAULT = SensorManager.SENSOR_DELAY_NORMAL;

    private static final int NOTIFICATION_ID = 1337;

    private CapoConnection mConnection;
    private ServiceHandler mServiceHandler;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private String mDevice;      // TODO: consider creating new class for devices
    private int mSensorType;
    private int mSensorDelay;

    private int mLinearSpeed;
    private final int[] mSpeed = new int[4];

    private static final int LINEAR_SPEED_MAX = 1000;
    private static final double THETA_EPS = 15 * Math.PI / 180;
    private static final double G = 9.81;


    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case CODE_START_TRANSMISSION:
                    startForeground(NOTIFICATION_ID, getServiceNotification("Not sampled yet"));
                    startTransmission();
                    break;
                case CODE_STOP_TRANSMISSION:
                    stopForeground(true);
                    stopTransmission();
                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        HandlerThread mSensorThread = new HandlerThread("SensorThread", Process.THREAD_PRIORITY_BACKGROUND);
        mSensorThread.start();

        final Looper mServiceLooper = mSensorThread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_REGISTER_RECEIVER.equals(action)) {
                mSensorType = intent.getIntExtra(EXTRA_SENSOR_TYPE, SENSOR_TYPE_DEFAULT);
                mSensorDelay = intent.getIntExtra(EXTRA_SENSOR_DELAY, SENSOR_DELAY_DEFAULT);
                mDevice = intent.getStringExtra(EXTRA_RECEIVER);
                mSensor = mSensorManager.getDefaultSensor(mSensorType);
                connect();
            } else if (ACTION_STOP_TRANSMISSION.equals(action)) {
                Message msg = mServiceHandler.obtainMessage();
                msg.arg1 = CODE_STOP_TRANSMISSION;
                mServiceHandler.sendMessage(msg);

            } else if (ACTION_STOP_ROBOT.equals(action)) {
                mLinearSpeed = 0;
            } else if (ACTION_POSITIVE_ACCELERATION.equals(action)) {
                mLinearSpeed += 100;
                if (mLinearSpeed > LINEAR_SPEED_MAX) mLinearSpeed = LINEAR_SPEED_MAX;
            } else if (ACTION_NEGATIVE_ACCELERATION.equals(action)) {
                mLinearSpeed -= 100;
                if (mLinearSpeed < -LINEAR_SPEED_MAX) mLinearSpeed = -LINEAR_SPEED_MAX;
            }

            Log.d(TAG, "Received action: " + action);
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void connect() {
        mConnection = new BluetoothRfcommConnection(this, mServiceHandler, mDevice);
        mConnection.connect();
    }

    private void startTransmission() {
        Log.i(TAG, "Transmission started");
        mSensorManager.registerListener(this, mSensor, mSensorDelay, mServiceHandler);
    }

    private void stopTransmission() {
        Log.i(TAG, "Transmission stopped");
        mSensorManager.unregisterListener(this);
        mConnection.disconnect();
    }

    private void updateNotification(String contentText) {
        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, getServiceNotification(contentText));
    }

    private Notification getServiceNotification(String contentText) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);

        return new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_bluetooth)
                .setContentTitle(getText(R.string.transmission_notification_title))
                .setContentText(contentText)
                .setContentIntent(pendingIntent)
                .build();
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        if (e.sensor.getType() == mSensorType) {
            final float gx = e.values[1];
            final float gy = -e.values[0];
            final float gz = -e.values[2];

            mSpeed[0] = mLinearSpeed;
            mSpeed[1] = mLinearSpeed;
            mSpeed[2] = mLinearSpeed;
            mSpeed[3] = mLinearSpeed;

            if (Math.abs(gz) < 3.5) {   // steering mode
                final double theta = Math.asin(gx / G);

                if (Math.abs(theta) > THETA_EPS) {
                    if (theta < 0) {    // turning left
                        mSpeed[0] *= 2;
                        mSpeed[2] *= 2;
                    } else {
                        mSpeed[1] *= 2;
                        mSpeed[3] *= 2;
                    }
                }
            }

            String speedData = String.format(Locale.US, "%d %d %d %d", mSpeed[0], mSpeed[1], mSpeed[2], mSpeed[3]);
            updateNotification(speedData);
            mConnection.write(speedData.getBytes());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // not used
    }
}
