package pl.edu.agh.sw16.capocontroller.connection;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import pl.edu.agh.sw16.capocontroller.SensorService;


public class BluetoothRfcommConnection extends CapoConnection {

    private static final String TAG = BluetoothRfcommConnection.class.getName();
    private static final UUID CAPO_UUID = UUID.fromString("c46303c4-22af-4a95-ad09-48568f6d6b4e");

    private final BluetoothAdapter mAdapter;
    private final String mAddress;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;


    public BluetoothRfcommConnection(Context context, Handler handler, String address) {
        super(context, handler);
        mAddress = address;

        mAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mAdapter == null) {
            // TODO: handle the case when device doesn't support bluetooth
            return;
        }
    }

    @Override
    public boolean connect() {
        if (!mAdapter.isEnabled()) {
            // TODO: handle the case when bluetooth is disabled
        }

        Set<BluetoothDevice> pairedDevices = mAdapter.getBondedDevices();

        BluetoothDevice device = null;
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice d : pairedDevices) {
                if (d.getAddress().equals(mAddress)) {
                    device = d;
                    break;
                }
            }
        }

        if (device != null) {
            synchronized (this) {
                Log.d(TAG, "Connected to " + device.getName());
                if (mConnectThread != null) {
                    mConnectThread.cancel();
                    mConnectThread = null;
                }

                if (mConnectedThread != null) {
                    mConnectedThread.cancel();
                    mConnectedThread = null;
                }

                mConnectThread = new ConnectThread(device);
                mConnectThread.start();
            }
            return true;
        } else {
            Log.e(TAG, "Unable to find the desired device");
            return false;
        }
    }

    @Override
    public boolean disconnect() {
        synchronized (this) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }

            if (mConnectedThread != null) {
                mConnectedThread.cancel();
                mConnectedThread = null;
            }
        }

        return true;
    }

    @Override
    public void write(byte[] buffer) {
        mConnectedThread.write(buffer);
    }

    private synchronized void startConnectedThread(BluetoothSocket socket, BluetoothDevice device) {
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        Message msg = mHandler.obtainMessage();
        msg.arg1 = SensorService.CODE_START_TRANSMISSION;
        mHandler.sendMessage(msg);
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            try {
                tmp = device.createRfcommSocketToServiceRecord(CAPO_UUID);
            } catch (IOException e) {
                Log.e(TAG, "Unable to create the socket.", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "ConnectThread starts");
            setName("ConnectThread");

            // Cancel discovery because it will slow down the connection
            mAdapter.cancelDiscovery();

            try {
                mmSocket.connect();
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: unable to connect", e);
                cancel();
                return;
            }
            Log.d(TAG, "ConnectThread: socket connected");

            synchronized (BluetoothRfcommConnection.this) {
                mConnectThread = null;
            }

            startConnectedThread(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Unable to close the socket", e);
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread created");
            mmSocket = socket;

            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Cannot get IO streams");
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG, "ConnectedThread starts");
            byte[] buffer = new byte[1024];
            int bytesRead;

            while (true) {
                try {
                    bytesRead = mmInStream.read(buffer);
                    if (bytesRead < 0) break;
                } catch (IOException e) {
                    Log.e(TAG, "Connection lost", e);
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Unable to close the socket", e);
            }
        }
    }
}
