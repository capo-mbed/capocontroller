package pl.edu.agh.sw16.capocontroller.connection;

import android.content.Context;
import android.os.Handler;


public abstract class CapoConnection {
    protected final Context mContext;
    protected final Handler mHandler;

    public CapoConnection(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }

    public abstract boolean connect();

    public abstract boolean disconnect();

    public abstract void write(byte[] buffer);
}
