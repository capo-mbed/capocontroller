package pl.edu.agh.sw16.capocontroller;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static final int OBTAIN_MAC_REQUEST = 1;

    private boolean readyToTransmission;
    private String deviceAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Switch sensorSwitch = (Switch) findViewById(R.id.sensor_switch);
        sensorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (readyToTransmission) {
                        startSensorService();
                    } else {
                        sensorSwitch.setChecked(false);
                        Toast.makeText(MainActivity.this, R.string.device_not_selected, Toast.LENGTH_LONG).show();
                    }
                } else {
                    stopSensorService();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect:
                Intent obtainMACAddressIntent = new Intent(this, ConnectActivity.class);
                startActivityForResult(obtainMACAddressIntent, OBTAIN_MAC_REQUEST);
                return true;
//            case R.id.settings:   // TODO: create settings
//                Intent showSettingsIntent = new Intent(this, SettingsActivity.class);
//                startActivity(showSettingsIntent);
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OBTAIN_MAC_REQUEST && resultCode == RESULT_OK) {
            deviceAddress = data.getStringExtra(ConnectActivity.EXTRA_SELECTED_DEVICE);

            TextView selectedDevice = (TextView) findViewById(R.id.selected_device_textview);
            selectedDevice.setText(String.format(getString(R.string.device_selected), deviceAddress));

            readyToTransmission = true;
        }
    }

    private void startSensorService() {
        Intent startServiceIntent = new Intent(this, SensorService.class)
                .setAction(SensorService.ACTION_REGISTER_RECEIVER)
                .putExtra(SensorService.EXTRA_RECEIVER, deviceAddress)
                .putExtra(SensorService.EXTRA_SENSOR_TYPE, Sensor.TYPE_GRAVITY)
                .putExtra(SensorService.EXTRA_SENSOR_DELAY, SensorManager.SENSOR_DELAY_GAME);

        startService(startServiceIntent);
    }

    private void stopSensorService() {
        startService(new Intent(this, SensorService.class)
                .setAction(SensorService.ACTION_STOP_TRANSMISSION));
    }

    public void accelerate(View view) {
        Intent intent = new Intent(this, SensorService.class);
        switch (view.getId()) {
            case R.id.plus_button:
                intent.setAction(SensorService.ACTION_POSITIVE_ACCELERATION);
                break;
            case R.id.minus_button:
                intent.setAction(SensorService.ACTION_NEGATIVE_ACCELERATION);
                break;
            case R.id.break_button:
                intent.setAction(SensorService.ACTION_STOP_ROBOT);
                break;
        }
        startService(intent);
    }
}
